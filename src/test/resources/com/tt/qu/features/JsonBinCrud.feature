@api
Feature: JsonBin CRUD
  In order to be provide functionality to store jsons
  As a developer
  I want to ensure basic CRUD operations work correctly

  @enabled
  Scenario: Should be able to create entity
    Given entity is Book with name "Clean code"
    When entity is created
    Then response code is 200
    And book is saved correctly
    And metadata is returned correctly

  @enabled
  Scenario Outline: Should be not able to create entity with incorrect json
    When trying to save incorrect json "<incorrectJson>"
    Then response code is 400
    And error message is "Invalid JSON. Please try again"
    Examples:
      | incorrectJson  |
      | not_a_json     |
      | { almost: true |

  @enabled
  Scenario: Should be able to read entity that was just created
    Given entity Book was created successfully
    When entity is read
    Then response code is 200
    And same book as was used in Create as returned

  @enabled
  Scenario Outline: Should return error upon reading using incorrect id
    When entity with incorrect id "<incorrectId>" is read
    Then response code is <code>
    And error message is "<errorMessage>"
    Examples:
      | incorrectId              | code | errorMessage                                       |
      | incorrect_format         | 422  | Invalid Record ID                                  |
      | 000000000000000000000000 | 404  | Bin not found or it doesn't belong to your account |

  @enabled
  Scenario: Should be able to update entity correctly
    Given entity Book was created successfully
    When entity is update with Book with name "now name does matter"
    Then response code is 200
    And Book was modified correctly

  @enabled
  Scenario Outline: Should return error upon updating entity using incorrect data
    Given entity Book was created successfully
    When entity with body "<body>" and incorrect id "<incorrectId>" is updated
    Then response code is <code>
    And error message is "<errorMessage>"
    Examples:
      | body        | incorrectId              | code | errorMessage                   | comment                                                    |
      | not_a_json  | incorrect_format         | 400  | Invalid JSON. Please try again |                                                            |
      | normal book | incorrect_format         | 422  | Invalid Record ID              |                                                            |
      | normal book | 000000000000000000000000 | 404  | Bin not found                  | could be a potential bug - inconsistency in error messages |

  @enabled
  Scenario: Should be able to delete entity correctly
    Given entity Book was created successfully
    When entity is deleted
    Then response code is 200
    And entity cannot be found by Id

  @enabled
  Scenario Outline: Should return error upon deleting entity using incorrect data
    Given entity Book was created successfully
    When entity with incorrect id "<incorrectId>" is deleted
    Then response code is <code>
    And error message is "<errorMessage>"
    Examples:
      | incorrectId              | code | errorMessage                                       |
      | incorrect_format         | 422  | Invalid Record ID                                  |
      | 000000000000000000000000 | 404  | Bin not found or it doesn't belong to your account |
