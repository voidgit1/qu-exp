package com.tt.qu.configuration;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.ConfigFactory;

@Config.Sources({"classpath:tst.properties"})
public interface EnvironmentConfig extends Config {
    EnvironmentConfig ENVIRONMENT_CONFIG = ConfigFactory.create(EnvironmentConfig.class, System.getProperties(), System.getenv());

    @Key("base.api.uri")
    String getApiBaseUri();

    @Key("api.key")
    String getApiKey();
}