package com.tt.qu.api.steps.support;

import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public final class CrudRequests {
    private CrudRequests() {
    }

    public static Response createEntity(Object body, RequestSpecification requestSpecification) {
        return given()
                .spec(requestSpecification)
                .body(body)
                .when().post("/b")
                .then().assertThat().header("content-type", "application/json; charset=utf-8")
                .extract().response();
    }

    public static Response readEntity(String id, RequestSpecification requestSpecification) {
        return given()
                .spec(requestSpecification)
                .when().get(String.format("/b/%s/latest", id))
                .then().assertThat().header("content-type", "application/json; charset=utf-8")
                .extract().response();
    }

    public static Response updateEntity(String id, Object bodyToUpdate, RequestSpecification requestSpecification) {
        return given()
                .spec(requestSpecification)
                .body(bodyToUpdate)
                .when().put(String.format("/b/%s", id))
                .then().assertThat().header("content-type", "application/json; charset=utf-8")
                .extract().response();
    }

    public static Response deleteEntity(String id, RequestSpecification requestSpecification) {
        return given()
                .spec(requestSpecification)
                .when().delete(String.format("/b/%s", id))
                .then().assertThat().header("content-type", "application/json; charset=utf-8")
                .extract().response();
    }
}
