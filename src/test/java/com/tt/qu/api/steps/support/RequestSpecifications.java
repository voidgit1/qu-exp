package com.tt.qu.api.steps.support;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static com.tt.qu.configuration.EnvironmentConfig.ENVIRONMENT_CONFIG;

public final class RequestSpecifications {
    private RequestSpecifications() {
    }

    public static RequestSpecification getRequestSpecification() {
        return new RequestSpecBuilder()
                .addFilter(new AllureRestAssured())
                .setContentType(ContentType.JSON)
                .setBaseUri(ENVIRONMENT_CONFIG.getApiBaseUri())
                .addHeader("X-Master-key", ENVIRONMENT_CONFIG.getApiKey())
                .build();
    }
}
