package com.tt.qu.api.steps;

import com.tt.qu.api.models.Book;
import com.tt.qu.api.models.response.ErrorResponse;
import com.tt.qu.api.models.response.partial.Metadata;
import com.tt.qu.api.steps.support.CrudRequests;
import com.tt.qu.api.steps.support.RequestSpecifications;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.assertj.core.api.Assertions.assertThat;

public final class CrudSteps {

    private RequestSpecification requestSpecification;
    private Book originalBook;
    private Book updatedBook;
    private Metadata originalMetadata;
    private Response response;

    @Before("@api")
    public void initBaseUrl() {
        requestSpecification = RequestSpecifications.getRequestSpecification();
    }

    @Given("entity is Book with name {string}")
    public void entityIsBookWithName(String name) {
        originalBook = createBookWithName(name);
    }

    @Given("entity is created")
    public void entityIsSavedAsJson() {
        response = CrudRequests.createEntity(originalBook, requestSpecification);
    }

    @Given("entity Book was created successfully")
    public void entityBookWasCreatedSuccessfully() {
        originalBook = createBookWithName("Name does not really matter");
        response = CrudRequests.createEntity(originalBook, requestSpecification);
        response.then().statusCode(200);
        originalMetadata = extractMetadata(response);
    }

    @When("trying to save incorrect json {string}")
    public void tryingToSaveIncorrectJsonIncorrectJson(String incorrectJson) {
        response = CrudRequests.createEntity(incorrectJson, requestSpecification);
    }

    @When("entity is read")
    public void entityIsRead() {
        response = CrudRequests.readEntity(originalMetadata.getId(), requestSpecification);
    }

    @When("entity with incorrect id {string} is read")
    public void entityWithIncorrectIdIncorrectIdIsRead(String incorrectId) {
        response = CrudRequests.readEntity(incorrectId, requestSpecification);
    }

    @When("entity is update with Book with name {string}")
    public void entityIsUpdateWithBookWithName(String newBookName) {
        updatedBook = Book.builder()
                .name(newBookName)
                .author(originalBook.getAuthor())
                .isbn(originalBook.getIsbn())
                .build();
        response = CrudRequests.updateEntity(originalMetadata.getId(), updatedBook, requestSpecification);
    }

    @When("entity with body {string} and incorrect id {string} is updated")
    public void entityWithBodyAndIncorrectIdIsUpdated(String body, String incorrectId) {
        var bodyToTest = body.equalsIgnoreCase("normal book")
                ? createBookWithName("Whatever")
                : body;
        response = CrudRequests.updateEntity(incorrectId, bodyToTest, requestSpecification);
    }

    @When("entity is deleted")
    public void entityIsDeleted() {
        response = CrudRequests.deleteEntity(originalMetadata.getId(), requestSpecification);
    }

    @When("entity with incorrect id {string} is deleted")
    public void entityWithIncorrectIdIsDeleted(String incorrectId) {
        response = CrudRequests.deleteEntity(incorrectId, requestSpecification);
    }

    @Then("same book as was used in Create as returned")
    public void sameBookAsWasUsedInCreateAsReturned() {
        var actualBook = extractBook(response);
        assertThat(actualBook).isEqualTo(originalBook);
    }

    @Then("Book was modified correctly")
    public void bookWasModifiedCorrectly() {
        var actualUpdatedBook = extractBook(response);
        assertThat(actualUpdatedBook).isEqualTo(updatedBook);
    }

    @Then("entity cannot be found by Id")
    public void entityCannotBeFoundById() {
        var statusCode = CrudRequests.readEntity(originalMetadata.getId(), requestSpecification)
                .getStatusCode();
        assertThat(statusCode).isEqualTo(404);
    }

    @Then("response code is {int}")
    public void responseCodeIs(int responseCode) {
        assertThat(response.getStatusCode()).isEqualTo(responseCode);
    }

    @Then("error message is {string}")
    public void errorMessageIs(String errorMessage) {
        var actualMessage = response.body().as(ErrorResponse.class).getMessage();
        assertThat(actualMessage).isEqualTo(errorMessage);
    }

    @Then("book is saved correctly")
    public void bookIsSavedCorrectly() {
        var actualBook = extractBook(response);
        assertThat(actualBook).isEqualTo(originalBook);
    }

    @Then("metadata is returned correctly")
    public void metadataIsReturnedCorrectly() {
        var actualMetadata = extractMetadata(response);

        assertThat(actualMetadata.getId()).isNotEmpty();

        var nowUtc = ZonedDateTime.now(ZoneId.of("UTC"));
        assertThat(actualMetadata.getCreatedAt()).isStrictlyBetween(
                nowUtc.minusMinutes(1), // increase this time when debugging :)
                nowUtc);

        assertThat(actualMetadata.getIsPrivate()).isTrue();
    }

    private static Book extractBook(Response response) {
        return response.jsonPath().getObject("record", Book.class);
    }

    private static Book createBookWithName(String name) {
        return Book.builder()
                .name(name)
                .isbn(RandomStringUtils.randomNumeric(13))
                .author("John Doe")
                .build();
    }

    private static Metadata extractMetadata(Response response) {
        return response.jsonPath().getObject("metadata", Metadata.class);
    }
}
