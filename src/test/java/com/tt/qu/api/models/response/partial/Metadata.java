package com.tt.qu.api.models.response.partial;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Metadata {
    private String id;
    private ZonedDateTime createdAt;
    @JsonProperty("private")
    private Boolean isPrivate;
}
