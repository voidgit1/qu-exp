package com.tt.qu;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(tags = "@enabled",
        plugin = {"pretty",
                "json:target/jsonReports/cucumber-report.json",
                "html:target/htmlReports/cucumber-report.html"})
public class RunCucumberTest {
}