### For local run

##### Preconditions:
1. JDK 11 is installed and in `%PATH%`
2. Install Allure as command line tool (for reporting) - https://docs.qameta.io/allure/#_installing_a_commandline
3. Optional: `Cucumber for Java` IDEA plugin for .feature files syntax highlight and to run/debug scenarios from IDEA.  

##### To run tests:
`mvn clean test`

##### To see Allure report (includes request/response pairs attached to the steps):
`allure serve .\target\allure-results\` (change path for non-Windows machine)

##### Cucumber report will be generated at:
`target\htmlReports`

### Deliverables:
1. Entry point/scenarios are in `*.feature` files (`.\src\test\resources\com\tt\qu\features\`)
2. https://gitlab.com/voidgit1/qu-exp/-/pipelines/427893132 - passed pipeline example
3. https://voidgit1.gitlab.io/-/qu-exp/-/jobs/1875967937/artifacts/target/site/allure-maven-plugin/index.html - Allure test report from the pipeline from item 2 above

### Things I would like to add if I had more time:
At least:
1. Authorization tests (amount depends on service(s) implementation)
2. Json saved and updated should be more complex
3. Better integration with GitLab CI (this is my first take on GitLab CI :) )
4. CrudSteps.java may be split
